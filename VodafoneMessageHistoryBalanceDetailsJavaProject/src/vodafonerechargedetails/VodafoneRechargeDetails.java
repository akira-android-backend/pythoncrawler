
package vodafonerechargedetails;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class VodafoneRechargeDetails {

    public static void main(String[] args) {
        try {
            
            String VdRchrg = "140";
            ResultSet rs;
            Connect c = new Connect();
            Connection conn = c.makeConnection();
            Statement st = conn.createStatement();
            String select_Stmt = "Select messageHistoryId, happenedAt ,messageText from MessageDetailsHistory where otherNumber ='"+VdRchrg+"';";
            rs = st.executeQuery(select_Stmt);
            while ( rs.next() ) 
            { 
                    Statement stt = conn.createStatement();
                    String balance = null;
                    String rechargeamt = null;
                    String id = rs.getString("messageHistoryId");
                    String daterecharge = rs.getString("happenedAt");
                    String str = rs.getString("messageText");
                    
                    str = str.toLowerCase();
                    String[] splittedstr = str.split(" ");
                    boolean CheckmrpPresence =  Arrays.asList(splittedstr).contains("mrp");

                    for (int i = 0 ; i<splittedstr.length;i++)
                    {
                        if (CheckmrpPresence)
                        {
                            if (("mrp:".equals(splittedstr[i])) || ("mrp".equals(splittedstr[i])))
                            {
                                i = i + 1;
                                if ("rs".equals(splittedstr[i]))
                                {
                                    i = i + 1;
                                    rechargeamt = splittedstr[i];
                                }
                                else if (splittedstr[i].matches("[r-s0-9.,]+"))
                                {
                                    rechargeamt = splittedstr[i];
                                }
                            }

                        }
                        else
                        {
                            if ("for".equals(splittedstr[i]))
                            {
                            i = i + 1;
                            if ("rs".equals(splittedstr[i]))
                            {
                                i = i + 1;
                                rechargeamt = splittedstr[i];
                            }
                            else if (splittedstr[i].matches("[r-s0-9.,]+"))
                            {
                                rechargeamt = splittedstr[i];
                            }
                            }
                        }
                        if (("bal".equals(splittedstr[i])))
                        {
                            
                            i = i + 1;
                            splittedstr[i] = splittedstr[i].replace(".transid","");
                            if ("rs".equals(splittedstr[i]))
                            {
                                i = i + 1;
                                balance = splittedstr[i];

                            }
                            else if (splittedstr[i].matches("[r-s0-9.,]+"))
                            {
                                balance = splittedstr[i];
                            }
                        }
                    }
                    if (rechargeamt != null){
                        rechargeamt = rechargeamt.replace("rs","");
                        rechargeamt = rechargeamt.replace(",","");
                    }
                    if (balance != null){
                        balance = balance.replace("rs","");
                        balance = balance.replace(",","");
                    }
                    stt.execute(" insert ignore into VodafoneRechargeDetails values ('"+id+"','"+daterecharge+"','"+rechargeamt+"' ,'"+balance+"' ) ;");
        
                }
            System.out.println("Success");
        }
        catch(SQLException e){
            System.out.println("Failed");
            e.printStackTrace();
        }
    }
}
     
   