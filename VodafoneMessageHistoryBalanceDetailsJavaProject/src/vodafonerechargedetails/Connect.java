
package vodafonerechargedetails;
import java.sql.SQLException;
import java.sql.*;

public class Connect {
    public Connect() throws SQLException{ makeConnection();} 
    private Connection con; 
    public  Connection makeConnection() throws SQLException 
    {
        if (con == null) 
            {
             con = DriverManager.getConnection(
                       "jdbc:mysql://localhost/test",
                       "root",
                       "root");
             }
        String stmt="create table if not exists VodafoneRechargeDetails ( ID varchar(100) Primary Key,DateOfRecharge varchar(80),RechargeAccount varchar(10),Balance Varchar(10));";

        Statement sql_smt = con.createStatement();

        sql_smt.execute(stmt);
       
         return con;
     }       
}
