
package networkoperatorandtelecomservicefinder;
/**
 *
 * @author Abi
 */
public class LocalSTDCallFinder {
    private String CallType;
    
    public LocalSTDCallFinder(String FromNumber, String ToNumber)
    {
        NetworkOperatorAndTelecomServiceFinder FromNumberObj = new NetworkOperatorAndTelecomServiceFinder(FromNumber);
        String FromNumberOperator = FromNumberObj.getNetworkOperator();
        String FromNumberService = FromNumberObj.getTelecomCircle();
        
        NetworkOperatorAndTelecomServiceFinder ToNumberObj = new NetworkOperatorAndTelecomServiceFinder(ToNumber);
        String ToNumberOperator = ToNumberObj.getNetworkOperator();
        String ToNumberService = ToNumberObj.getTelecomCircle();
        
        if(FromNumberService.equals("invalid") || ToNumberService.equals("invalid")){
            System.out.println("LandLine");
            
        }
        else{
            if(FromNumberService.equals(ToNumberService)){
                this.CallType = "Local";
            }
            else
                this.CallType = "STD";
            }
        }
    public String getCallType(){
        return this.CallType;
    }
    
}
