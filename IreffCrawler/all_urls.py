'''
	Run 1st.
	Crawl the Ireff Website and collect all the network operators, telecom circle and url for each combination of
	operator and circle
'''
from bs4 import BeautifulSoup
from mysql.connector import MySQLConnection, Error
from mysql.connector import (connection)
import urllib2
import re
import mysql.connector
import config
import sqlscripts


try:
		url="http://www.ireff.in/"
		page=urllib2.urlopen(url)
		soup = BeautifulSoup(page.read())						  
		cursor = sqlscripts.db.cursor()
		ids=1
		link_list=[]
		link = "http://www.ireff.in/plans/"
		data = cursor.fetchone()
		selectcircle = soup.find('select' , id ="mcircle")
		selectservice = soup.find('select' , id ="mservice")
		for option in selectcircle.find_all('option'):
			circle_value = option['value']
			circle_text = option.text
			if circle_text == "Andhra Pradesh & Telangana" :
				circle_text = "Andra Pradesh & Telangana"
			for option in selectservice.find_all('option'):
				service_value = option['value']
				service_text = option.text
				if circle_value and service_value:
					link = "http://www.ireff.in/plans/" + service_text.replace(" ","-") + "/" + re.sub(r'\W+', '-', circle_text)
					link = link.lower()
					link_list.append(link)
					cursor.execute('insert ignore into %s values("%d","%s","%s","%s") ON DUPLICATE KEY UPDATE ID = values(ID) , service = values(service) ,\
					circle = values(circle) , links = values(links)' % \
					(config.url_tab , ids , service_text , circle_text , link) )
					ids = ids + 1
					
				link = " "

	
	
except URLError as log_error:
	sqlscripts.db.rollback()
	print "Make sure net connections are available"
												
except Exception as log_error:
	sqlscripts.db.rollback()
	print "error in code"
	
sqlscripts.db.commit()
sqlscripts.db.close()
