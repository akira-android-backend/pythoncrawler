'''
	No need to run.Because this script is imported in other script, so when we run that script all imported scripts will run first.
	Sqlscript to create database and all tables
'''

from mysql.connector import (connection)
import mysql.connector
import config
import time
from datetime import date, datetime
try:
	db = mysql.connector.connect(user='root', password='root',)					
	cursor = db.cursor()
	cursor.execute('CREATE DATABASE IF NOT EXISTS test2;')
	cursor.execute('USE test2;')
	cursor.execute(config.create_url_tab)
	cursor.execute(config.create_plan_details_tab)
	create_time = time.strftime("%Y-%m-%d %H:%M:%S")
	db.commit()

except mysql.connector.Error as err:
	print err