'''
	No need to run .Create script for all tables along with table name
'''


url_tab = "all_url"
plan_details_tab = "plan_details"


create_url_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
    ID INT AUTO_INCREMENT PRIMARY KEY, \
    service VARCHAR(32), \
    circle VARCHAR(50), \
	links VARCHAR(100) \
)""" % url_tab

create_plan_details_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
	URLID INT , \
	prodid INT PRIMARY KEY , \
    price VARCHAR(20), \
    validity VARCHAR(32), \
	talktime INT(10) , \
	tags VARCHAR(100), \
    benefits VARCHAR(200), \
	created_on DATETIME ,\
	updated_on DATETIME,  \
	FOREIGN KEY (URLID) REFERENCES all_url(ID)
)""" % plan_details_tab



							  