'''
	Get all the url from all_urls.
	Crawl the url and get brief data about each plan.
'''
from bs4 import BeautifulSoup
from datetime import date, datetime
import plan_details_linklist
import time
import re
import urllib2
import config
import sqlscripts
from mysql.connector import MySQLConnection, Error
from mysql.connector import (connection)


count = 0

try:
	LinkCursor = sqlscripts.db.cursor()
	LinkCursor.execute('select * from %s limit 3' % config.url_tab)
	LinkResults = LinkCursor.fetchall()
	for data in LinkResults:
		urlid = data[0]
		link = data[3]
		print link
		page=urllib2.urlopen(link)
		soup = BeautifulSoup(page.read())
		cursor = sqlscripts.db.cursor()
		lists = ["Topup","SMS","2G","3G","Local","STD","ISD","Other"]
		for list in lists:
			for divss in soup.findAll("div", {"id": lists}):
				for tabless in divss.findAll("table"):
					for tbodyss in tabless.findAll("tbody" ):
						for trss in tbodyss.findAll("tr"):
							prodid = trss['prodid']
							for tdss in trss.findAll("td"):
								if count == 0:
									price = tdss.text
									count = count + 1
								elif count == 1:
									validity = tdss.text
									count = count + 1
								elif count == 2:
									talktime = re.escape(tdss.text)
									count = count + 1
								elif count == 3:
									tags = tdss.text
									count = count + 1
								elif count == 4:
									benefits = tdss.text
									benefits = re.escape(benefits)
									count = 0
							updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
							cursor.execute('insert IGNORE into %s values("%s","%s"," %s "," %s "," %s "," %s ", "%s" , "%s" , "%s") ON DUPLICATE KEY UPDATE \
							price = values(price) , validity = values(validity) , talktime = values(talktime) , \
							tags = values(tags) , benefits = values(benefits) , updated_on = values(updated_on)' % \
							(config.plan_details_tab , urlid, prodid, price, validity, talktime, tags, benefits , sqlscripts.create_time , updated_on) )
		
except URLError as log_error:
	sqlscripts.db.rollback()
	print "Make sure net connections are available"
												
except Exception as log_error:
	sqlscripts.db.rollback()
	print "error in code"		

sqlscripts.db.commit()
sqlscripts.db.close()