from mysql.connector import (connection)
import mysql.connector
import config
import time
from datetime import date, datetime
try:
	db = mysql.connector.connect(user='root', password='root')					
	cursor = db.cursor()
	cursor.execute('CREATE DATABASE IF NOT EXISTS test;')
	cursor.execute('USE test;')
	cursor.execute(config.create_url_tab)
	cursor.execute(config.create_mart_url_tab)
	cursor.execute(config.create_plan_details_tab)
	cursor.execute(config.create_mart_plan_details_tab)
	cursor.execute(config.create_baseplan_tab)
	cursor.execute(config.create_mobile_number_tab)
	create_time = time.strftime("%Y-%m-%d %H:%M:%S")
	db.commit()

except mysql.connector.Error as err:
	print err