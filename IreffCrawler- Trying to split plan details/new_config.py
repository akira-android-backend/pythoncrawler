url_tab = "urls_table"
mart_url_tab = "mart_url_table"
plan_details_tab = "plan_detail_table"
#plan_details_tab = "plan_details_table"
#mart_plan_details_tab = "mart_plan_details_table"
mart_plan_details_tab = "mart_plan_table"
baseplan_tab = "baseplan_table"
Mobile_Number_tab = "Mobile_Number_table"

create_url_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
    URLID INT AUTO_INCREMENT PRIMARY KEY, \
	service_id VARCHAR(20),\
    service VARCHAR(32), \
	circle_id VARCHAR(20),\
    circle VARCHAR(50), \
	links VARCHAR(100) \
)""" % url_tab

create_plan_details_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
	URLID INT , \
	prodid INT PRIMARY KEY , \
    price VARCHAR(20), \
    validity VARCHAR(32), \
	talktime INT , \
	tags VARCHAR(100), \
    benefits VARCHAR(2000), \
	created_on DATETIME ,\
	updated_on DATETIME,  \
	FOREIGN KEY (URLID) REFERENCES urls_table(URLID) \
)""" % plan_details_tab


create_mart_url_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
    URLID INT PRIMARY KEY, \
    service_id VARCHAR(32), \
    circle_id VARCHAR(50), \
	links VARCHAR(100) \
)""" % mart_url_tab

create_mart_plan_details_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
	URLID INT, \
	Prodid INT PRIMARY KEY , \
	Service_Id VARCHAR(50),\
	Circle_Id VARCHAR(50),\
	Price VARCHAR(20), \
	Validity VARCHAR(32), \
	Talktime INT, \
	IsTopup INT(1),\
	IsLocal INT(1), \
	Is2G INT(1),\
	Is3G INT(1),\
	IsRoaming INT(1),\
	IsSms INT(1),\
	IsSTD INT(1),\
	IsISD INT(1),\
	IsNight INT(1),\
	IsFRC INT(1), \
	count int,\
	TopupBenefits VARCHAR(1000),\
	Local_CallBenefits VARCHAR(1000),\
	Local_SmsBenefits VARCHAR(1000),\
	T2GBenefits VARCHAR(1000),\
	T3GBenefits VARCHAR(1000),\
	T2Gand3G_Benefits VARCHAR(1000),\
	RoamingBenefits VARCHAR(1000),\
	
	STD_CallBenefits VARCHAR(1000),\
	STD_SmsBenefits VARCHAR(1000),\
	LocalandSTD_CallBenefits VARCHAR(1000),\
	LocalandSTD_SmsBenefits VARCHAR(1000),\
	ISDBenefits VARCHAR(1000),\
	NightBenefits VARCHAR(1000),\
	FRCBenefits VARCHAR(1000),\
	OtherBenefits VARCHAR(1000),\
	ActivationCode VARCHAR(1000)\
)""" % mart_plan_details_tab


create_baseplan_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
	URLID INT, \
	Prodid INT PRIMARY KEY , \
	Service_Id VARCHAR(50),\
	Circle_Id VARCHAR(50),\
	Price VARCHAR(20), \
	Validity VARCHAR(32), \
	Talktime INT, \
	Local_Call_Cost VARCHAR(1000),\
	Local_SMS_Cost VARCHAR(1000),\
	STD_Call_Cost VARCHAR(1000),\
	STD_SMS_Cost VARCHAR(1000),\
	DataCost VARCHAR(1000),\
	RoamingCost VARCHAR(1000),\
	ISDCost VARCHAR(1000),\
	OtherBenefits VARCHAR(1000)\
)""" % baseplan_tab



create_mobile_number_tab = """CREATE TABLE IF NOT EXISTS %s \
( \
	Code int(10) PRIMARY KEY  , \
	Network_Operator_Code VARCHAR(50), \
    Telecome_Circle_Code VARCHAR(50)\
)""" % Mobile_Number_tab






							  