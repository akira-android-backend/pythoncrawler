
package connect1;
import java.sql.SQLException;

import java.sql.*;
public class Connect1 {
    public Connect1() throws SQLException{
        makeConnection();
    }
    private Connection con;  

     public  Connection makeConnection() throws SQLException {
        if (con == null) {
           
             con = DriverManager.getConnection(
                       "jdbc:mysql://localhost/test",
                       "root",
                       "root");
         }
        String stmt="create table if not exists AirtelRechargeDetails (Id varchar(100) Primary Key ,DateOfRecharge varchar(80),RechargeAccount varchar(10),Balance Varchar(10));";

        Statement sql_smt = con.createStatement();

        sql_smt.execute(stmt);
        //System.out.println(rs);
         return con;
     }  

     
}
