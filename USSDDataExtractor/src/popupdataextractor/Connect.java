
package popupdataextractor;
import java.sql.SQLException;
import java.sql.*;

public final class Connect {
    public Connect() throws SQLException{ makeConnection();} 
    private Connection con; 
    public  Connection makeConnection() throws SQLException 
    {
        if (con == null) 
            {
             con = DriverManager.getConnection(
                       "jdbc:mysql://localhost/test",
                       "root",
                       "root");
             System.out.println(" connected");
             
             }
        else
        {
            System.out.println("not connected");
        }
        //String stmt="create table if not exists PopupExtractedData ( ID varchar(100) Primary Key,CallDuration varchar(80),CallCost Varchar(50),CurrentBalance Varchar(50),MobilePlan Varchar(100);";
           
          String stmt1="create table if not exists USSDExtractedData ( ID varchar(100) Primary Key,UssdMsg varchar(500),CurrentBalance Varchar(100), Validity Varchar(100));";
           Statement sql_smt1 = con.createStatement();
           sql_smt1.execute(stmt1);
         return con;
     }       
}
