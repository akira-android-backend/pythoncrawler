/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package popupdataextractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class USSDDataExtractor {
    private String Balance = null;
    private String Validity;
    private String CallBalance;
    private String DataBalance;
    private String CallValidity;
    private String DataValidity;

    public USSDDataExtractor(String ussd_msg) {
        ussd_msg = ussd_msg.toLowerCase();
        ussd_msg = ussd_msg.replace(" ", "");
        ussd_msg = ussd_msg.replace("[", "").replace("]", "");
        
        ArrayList<String> balance_list_prefix = new ArrayList<>(Arrays.asList("main","bal","bal:","mainbal","mainbal:","bal-left","bal-left:",
                                            "availablebal","availablebal:","availablebalance","availablebalance:","accountbalance",
                                            "accountbalance-","accbal","accbal:","balance","balance:","databal","mobileinternetbalanceare",
                                            "mobileinternetbalanceare:","databal:","balanceis","3gvol","2gvol",
                                            "balancefor(.*)is")); 
        
        ArrayList<String> balance_list_suffix = new ArrayList<>(Arrays.asList("leftout")); 
                                       
        ArrayList<String> validity_list = new ArrayList<>(Arrays.asList("val","val:","validity","validity:","expiry",
                                            "accexpdate","expiry:","expirydateis","exp:","expirydate","till",
                                            "valiity","accountexpdate"));
       // ArrayList<String> data_balance_list = new ArrayList<>(Arrays.asList("databal","mobileinternetbalanceare","mobileinternetbalanceare:",
         //                                   "databal:"));
        
        
        
        if(Balance == null)
        {
            /*
            Used to extract balance from Special case where phone number comes in ussd msg like
            [Balance for 7406933839 is Rs. 85.58. Account Exp Date 25/09/2021 23:59.]
            */
            Pattern balance_pattern = Pattern.compile("(balancefor)+\\d{10}(is)[(rs)+[0-9.=]+(inr)(mb)(gb)(atm)(kb)?+]+");
            Matcher balance_matcher = balance_pattern.matcher(ussd_msg);
            if(balance_matcher.find())
                {
                    Balance = balance_matcher.group(0);
                    Balance = Balance.replaceAll("[a-z:&&[^rsatmbgk]]","");
                    Balance = Balance.replaceAll("\\d{10}","");
                         
                } 
        }
        
        if(Balance == null)
        {
            for(String balancelist : balance_list_prefix) // Finds the balance which is followed by the keywords in balance_list array
            {
                if(ussd_msg.contains(balancelist))
                        {
                        String balance = "[(rs)+[0-9.=]+(inr)(mb)(gb)(atm)(kb)?+]+";
                        balance = "(" + balancelist + ")" + balance;//Forms regex pattern for balance like "(bal)+[0-9.[rsmgbgatm]?+]+"

                        Pattern balance_pattern = Pattern.compile(balance);
                        Matcher balance_matcher = balance_pattern.matcher(ussd_msg);
                        if(balance_matcher.find())
                                {
                                        Balance = balance_matcher.group(0);
                                        Balance = Balance.replaceAll(balancelist, ""); // remove the keyword     
                                } 
                }
            }//for ends
        }
        if(Balance == null)
        {
            for(String balancelist : balance_list_suffix) //Finds the balance which is prefix to the keywords in the list
            {
                if(ussd_msg.contains(balancelist))
                    {
                    String balance = "[(rs)+[0-9.=]+(inr)(kb)(gb)(mb)(amt)?]+";
                    balance = balance + "(" + balancelist + ")";//forming regex pattern for balance like "[(rs)+[0-9.=]+(inr)(kb)(gb)(mb)(amt)?]+(leftout)"
                    Pattern balance_pattern = Pattern.compile(balance);
                    Matcher balance_matcher = balance_pattern.matcher(ussd_msg);
                    if(balance_matcher.find())
                        {
                            Balance = balance_matcher.group(0);
                            Balance = Balance.replaceAll(balancelist, ""); // remove the keyword    
                        } 
                    }
            }//for ends
        }
        
      
        for(String validitylist : validity_list)
            {
             String validity_format = "(" + validitylist + ")" + ("(:)?+(-)?+\\d{4}-\\d{2}-\\d{2}");
             Pattern validity_pattern = Pattern.compile(validity_format);//Forms the patterns for date like 1994-05-15
             // Forming the patterns for date like (15-02-2015)
             String validity_format1 = "(" + validitylist + ")" + ("(:)?+(-)?+\\d{2}-\\d{2}-\\d{4}");
             Pattern validity_pattern1 = Pattern.compile(validity_format1);//Forms the patterns for date like 15-05-1994
             
             String validity_format2 = "(" + validitylist + ")" + ("(:)?+(-)?+\\d{2}[/\\\\]\\d{2}[/\\\\]\\d{4}");
             Pattern validity_pattern2 = Pattern.compile(validity_format2);// Forms the patterns for date like 15//05//1994
             
             String validity_format3 = "(" + validitylist + ")" + ("(:)?+(-)?+\\d{2}[a-z]+\\d{2}");
             Pattern validity_pattern3 = Pattern.compile(validity_format3);// Forms the patterns for date like 27Apr2015
             
             String validity_format4 = "(" + validitylist + ")" + ("[a-z]+\\d{1,2}[,]\\d{4}");
             Pattern validity_pattern4 = Pattern.compile(validity_format4);// Forms the patterns for date like Dec12,2015
             
             
             Matcher validity_matcher = validity_pattern.matcher(ussd_msg);
             Matcher validity_matcher1 = validity_pattern1.matcher(ussd_msg);
             Matcher validity_matcher2 = validity_pattern2.matcher(ussd_msg);
             Matcher validity_matcher3 = validity_pattern3.matcher(ussd_msg);
             Matcher validity_matcher4 = validity_pattern4.matcher(ussd_msg);
             if(validity_matcher.find()){
                 Validity = validity_matcher.group(0);
                 Validity = Validity.replace(validitylist, "");
             }
             else if(validity_matcher1.find()){
                 Validity = validity_matcher1.group(0);
                 Validity = Validity.replace(validitylist, "");
             }
             else if(validity_matcher2.find()){
                 Validity = validity_matcher2.group(0); 
                 Validity = Validity.replace(validitylist, "");
             }
             else if(validity_matcher3.find()){
                 Validity = validity_matcher3.group(0);  
                 Validity = Validity.replace(validitylist, "");
             }
              else if(validity_matcher4.find()){
                 Validity = validity_matcher4.group(0);  
                 Validity = Validity.replace(validitylist, "");
             }
             
             
                }//for ends

        if(Balance == null){
            System.out.println("invalid Ussd code");
        } 
        else if ((Balance.contains("gb"))|| (Balance.contains("mb"))) // Make it data balance if it contains mb or gb
            {
                Balance = Balance.replaceAll("[a-z:&&[^atmkgb]]","");
                if (Balance.startsWith("."))
                           Balance = Balance.replaceFirst(".", "");
                if (Balance.endsWith("."))
                        {
                        int len = Balance.length(); //get the length of balance
                        String last_char = Balance.substring(len-1); // extract the last character in string and 
                        Balance = Balance.substring(0, len-1);
                        }
                if (Balance.endsWith("b"))//Make it databalance if it ends with "b" i.e mb,gb,kb
                {
                        DataBalance = Balance;
                }
                else if(Balance.endsWith("m"))//Make it databalance if it ends with "m" i.e atm
                {
                        DataBalance = Balance;
                }
                else //Else remove unwanted characters at last
                {
                        int len = Balance.length(); //get the length of balance
                        String last_char = Balance.substring(len-1); // extract the last character in string and 
                        Balance = Balance.substring(0, len-1);
                }
                Validity = extractValidity(Validity);
                DataBalance = Balance;
                DataValidity = Validity;
            }
        else // make it as call balance
            { 
            Balance = Balance.replaceAll("[a-z:-]","");
            if (Balance.startsWith("."))
                   Balance = Balance.replaceFirst(".", "");
            if (Balance.endsWith("."))
                {
                int len = Balance.length(); //get the length of balance
                String last_char = Balance.substring(len-1); // extract the last character in string and 
                Balance = Balance.substring(0, len-1);
                        }
            CallBalance = Balance;
            Validity = extractValidity(Validity);
            CallValidity = Validity;
            }    
    } 
    
    String extractBalance(String balance)
        {
            int len = balance.length(); //get the length of balance
            char[] last_char_array = balance.substring(len-1).toCharArray(); // extract the last character in string and 
                                                                             //insert it in char arra                                                                 
            char last_char = last_char_array[0]; //(string cant be converted directly into char)
            while (!Character.isDigit(last_char))
                {
                 balance = balance.substring(0,len-1);
                 len = balance.length();
                 last_char_array = balance.substring(len-1).toCharArray();
                 last_char = last_char_array[0];
                }
        return balance;
     }
    
    String extractValidity(String validity)
        {
           if(validity != null){
               if (validity.startsWith(":"))
                   validity = validity.replaceFirst(":", "");
               else if(validity.startsWith("-"))
                   validity = validity.replaceFirst("-", "");  
            } 
        return validity;
     }
    
    public String getBalance(){
        return Balance;
    }
    public String getValidity(){
        return Validity;   
    }
    
    public String getCallBalance() {
        return CallBalance;
    }

    public String getDataBalance() {
        return DataBalance;
    }

    public String getCallValidity() {
        return CallValidity;
    }

    public String getDataValidity() {
        return DataValidity;
    }
    
}
