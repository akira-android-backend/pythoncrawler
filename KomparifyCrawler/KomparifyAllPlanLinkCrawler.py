'''
	Run 3rd
	Extract the link from RegionOperatorNamesTable and crawl each website 
	And collect brief detail about every plan along with the corresponding url for each plan
'''
from bs4 import BeautifulSoup
from mysql.connector import MySQLConnection, Error
import urllib2
import re
import KomparifyBaseplanConfig
import KomparifyBasePlanSqlscripts
import KomparifyLogStatusCodes
import time
from urllib2 import HTTPError, URLError

StartTime = time.strftime("%Y-%m-%d %H:%M:%S")


PlanId = 1
count = 0
row_count = 0
row_count +=1
print row_count

try:
	cursor = KomparifyBasePlanSqlscripts.db.cursor()
except Exception, log_error:
	print log_error

cursor.execute('insert ignore into %s (StartTime , TaskId, LogStatusId) values ("%s","%d","%s")'%\
												(KomparifyBaseplanConfig.Log,StartTime,KomparifyLogStatusCodes.ko_ldplanlink_task_id,KomparifyLogStatusCodes.started_status_id) )

cursor.execute('SELECT LAST_INSERT_ID()')
current_log_ids = cursor.fetchone();
current_log_id = current_log_ids[0]


try :
	error_flag = 0
	
	RegionOperatorCursor = KomparifyBasePlanSqlscripts.db.cursor()
	RegionOperatorCursor.execute('select * from %s' % KomparifyBaseplanConfig.RegionOperatorNames)
	RegionOperatorNameTableResults = RegionOperatorCursor.fetchall()
	
	log_detail = "Retriving data from the RegionOperatorNamesTablle " 									
	cursor.execute('insert ignore into %s (LogId,LogDescription,ErrorFlag) values("%d","%s","%d")'%\
												(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
	
	for result_rows in RegionOperatorNameTableResults:
		UrlId = result_rows[0]
		Operator = result_rows[2]
		Circle = result_rows[4]
		link = result_rows[5]
		LinkArray =[]
		PrepaidLink = link + "?type=prepaid"
		PostpaidLink = link + "?type=postpaid"
		LinkArray.append(PrepaidLink)
		LinkArray.append(PostpaidLink)
		LinkCount = 0
		for link in LinkArray:
			cursor.execute('update %s set LogStatusId = "%d" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.running_status_id,current_log_id) )
			print link
			log_detail = "Inserting all the plans for " + Operator + " - " + Circle
			
			error_flag = 0
			cursor.execute('insert ignore into %s(LogId, LogDescription,ErrorFlag) values("%d","%s","%d")'%\
			(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
			
			page=urllib2.urlopen(link)
			soup = BeautifulSoup(page.read())
			if LinkCount == 1:
				PlanType = "Postpaid"
			else:
				PlanType = "Prepaid"
			for divs in soup.findAll("div", {"id": "content"}):
				for divss in divs.findAll("div", {"class": "maintopup "}):
					TopupType = divss.find("h3",{"class": "categoryheader"}).text
					div_prescence = divss.find("div",{"align":"center"})
					if div_prescence:
						SubLinkData = div_prescence.find("a")
						SubLink = "https://www.komparify.com" + SubLinkData["href"]
						InnerPage=urllib2.urlopen(SubLink)
						soups = BeautifulSoup(InnerPage.read())
						for div1 in soups.findAll("div", {"id": "home-content-container"}):
								for div2 in div1.findAll("div",{"id": "content"}):
									for tabless in div2.findAll("table"):
										for trss in tabless.findAll("tr"):
											for tdss in trss.findAll("td"):
													if count == 0:
														PlanName = tdss.text
														PlanUrl = tdss.find("a")
														Url = PlanUrl['href']
														PlanUrl = "https://www.komparify.com" + Url
														PlanArray = PlanUrl.split("/")
														PlanId = PlanArray[-1]
														count = count + 1
													elif count == 1:
														BasePrice = tdss.text
														BasePrice = BasePrice.replace(u'\u20b9', u'')
														count = count + 1
													elif count == 2:
														Validity = tdss.text
														count = count + 1
													elif count == 3:
														BenefitsText = tdss.text
														count = count + 1
													elif count == 4:
														count = 0
											updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
											cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s",\
											"%s","%s","%s") ON DUPLICATE KEY UPDATE\
											PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
											Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
											updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,
											Operator,Circle,PlanType ,\
											TopupType, BasePrice, Validity, BenefitsText,PlanUrl, KomparifyBasePlanSqlscripts.create_time , updated_on) )
											row_count +=1
											
											
						
					else:
						for tabless in divss.findAll("table"):
							for trss in tabless.findAll("tr"):
								for tdss in trss.findAll("td"):
										if count == 0:
											PlanName = tdss.text
											PlanUrl = tdss.find("a")
											Url = PlanUrl['href']
											PlanUrl = "https://www.komparify.com" + Url
											PlanArray = PlanUrl.split("/")
											PlanId = PlanArray[-1]
											count = count + 1
										elif count == 1:
											BasePrice = tdss.text
											BasePrice = BasePrice.replace(u'\u20b9', u'')
											count = count + 1
										elif count == 2:
											Validity = tdss.text
											count = count + 1
										elif count == 3:
											count = 0
								BenefitsText = " "
								updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
								cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s", "%s","%s","%s") ON DUPLICATE KEY UPDATE\
								PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
								Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
								updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,Operator,Circle,PlanType ,\
									TopupType, BasePrice, Validity, BenefitsText,PlanUrl, KomparifyBasePlanSqlscripts.create_time , updated_on) )
								row_count +=1
													
				
				for divss in divs.findAll("div", {"class": "leftopup"}):
					TopupType = divss.find("h3",{"class": "categoryheader"}).text
					div_prescence = divss.find("div",{"align":"center"})
					if div_prescence:
						SubLinkData = div_prescence.find("a")
						SubLink = "https://www.komparify.com" + SubLinkData["href"]
						InnerPage=urllib2.urlopen(SubLink)
						soups = BeautifulSoup(InnerPage.read())
						for div1 in soups.findAll("div", {"id": "home-content-container"}):
								for div2 in div1.findAll("div",{"id": "content"}):
									for tabless in div2.findAll("table"):
										for trss in tabless.findAll("tr"):
											for tdss in trss.findAll("td"):
													if count == 0:
														PlanName = tdss.text
														Plan = tdss.find("a")
														Url = Plan['href']
														PlanUrl = "https://www.komparify.com" + Url
														PlanArray = PlanUrl.split("/")
														PlanId = PlanArray[-1]
														count = count + 1
														
													elif count == 1:
														BasePrice = tdss.text
														BasePrice = BasePrice.replace(u'\u20b9', u'')
														count = count + 1
													elif count == 2:
														Validity = tdss.text
														count = count + 1
													elif count == 3:
														BenefitsText = tdss.text
														count = count + 1
													elif count == 4:													
														count = 0
											updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
											cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s", "%s","%s","%s") ON DUPLICATE KEY UPDATE\
											PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
											Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
											updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,
											Operator,Circle,PlanType ,\
											TopupType, BasePrice, Validity, BenefitsText,PlanUrl, KomparifyBasePlanSqlscripts.create_time , updated_on) )
											row_count +=1

						
					else:
						for tabless in divss.findAll("table"):
							for trss in tabless.findAll("tr"):
								for tdss in trss.findAll("td"):
										if count == 0:
											PlanName = tdss.text
											PlanUrl = tdss.find("a")
											Url = PlanUrl['href']
											PlanUrl = "https://www.komparify.com" + Url
											PlanArray = PlanUrl.split("/")
											PlanId = PlanArray[-1]
											count = count + 1
										elif count == 1:
											BasePrice = tdss.text										
											BasePrice = BasePrice.replace(u'\u20b9',u'')
											count = count + 1
										elif count == 2:
											Validity = tdss.text
											count = count + 1										
										elif count == 3:
											count = 0
								BenefitsText = " "
								updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
								cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s", "%s","%s","%s") ON DUPLICATE KEY UPDATE\
								PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
								Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
								updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,Operator,Circle,PlanType ,\
									TopupType, BasePrice, Validity, BenefitsText,PlanUrl, KomparifyBasePlanSqlscripts.create_time , updated_on) )
								row_count +=1

				for divss in divs.findAll("div", {"class": "rightopup"}):
					TopupType = divss.find("h3",{"class": "categoryheader"}).text
					div_prescence = divss.find("div",{"align":"center"})
					if div_prescence:
						SubLinkData = div_prescence.find("a")
						SubLink = "https://www.komparify.com" + SubLinkData["href"]
						InnerPage=urllib2.urlopen(SubLink)
						soups = BeautifulSoup(InnerPage.read())
						for div1 in soups.findAll("div", {"id": "home-content-container"}):
								for div2 in div1.findAll("div",{"id": "content"}):
									for tabless in div2.findAll("table"):
										for trss in tabless.findAll("tr"):
											for tdss in trss.findAll("td"):
													if count == 0:
														PlanName = tdss.text
														PlanUrl = tdss.find("a")
														Url = PlanUrl['href']
														PlanUrl = "https://www.komparify.com" + Url
														PlanArray = PlanUrl.split("/")
														PlanId = PlanArray[-1]
														count = count + 1
													elif count == 1:
														BasePrice = tdss.text														
														BasePrice = BasePrice.replace(u'\u20b9',u'')
														count = count + 1
													elif count == 2:
														Validity = tdss.text
														count = count + 1
													elif count == 3:
														BenefitsText = tdss.text
														count = count + 1
													elif count == 4:
														count = 0
											updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
											cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s", "%s","%s","%s") ON DUPLICATE KEY UPDATE\
								PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
								Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
								updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,Operator,Circle,PlanType ,\
									TopupType, BasePrice, Validity, BenefitsText,PlanUrl, KomparifyBasePlanSqlscripts.create_time , updated_on) )
											row_count +=1
						
						
						
					else:
						for tabless in divss.findAll("table"):
							for trss in tabless.findAll("tr"):
								
								for tdss in trss.findAll("td"):
										if count == 0:
											PlanName = tdss.text
											PlanUrl = tdss.find("a")
											Url = PlanUrl['href']
											PlanUrl = "https://www.komparify.com" + Url
											PlanArray = PlanUrl.split("/")
											PlanId = PlanArray[-1]
											count = count + 1
										elif count == 1:
											BasePrice = tdss.text
											BasePrice = BasePrice.replace(u'\u20b9',u'')
											count = count + 1
										elif count == 2:
											Validity = tdss.text
											count = count + 1
										elif count == 3:
											count = 0
								BenefitsText = " "				
								updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
								cursor.execute('insert ignore into %s values("%d","%s","%s","%s", "%s","%s","%s", "%s","%s","%s", "%s","%s","%s") ON DUPLICATE KEY UPDATE\
								PlanName = values(PlanName),PlanID = values(PlanID),TopupType = values(TopupType),BasePrice = values(BasePrice),\
								Validity = values(Validity),Benefits_text =  values(Benefits_text),PlanUrl = values(PlanUrl),\
								updated_on = values(updated_on)' % (KomparifyBaseplanConfig.AllPlanLinks, UrlId ,PlanId, PlanName,Operator,Circle,PlanType ,\
									TopupType, BasePrice, Validity, BenefitsText,PlanUrl,KomparifyBasePlanSqlscripts.create_time , updated_on) )
								row_count +=1
											
			LinkCount = LinkCount + 1
			
		EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
		cursor.execute('update %s set LogStatusId = "%d" ,EndTime = "%s" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.success_status_id,EndTime,current_log_id) )
												
		log_detail = str(row_count) + " " + "records got refreshed"										
		error_flag = 0
		cursor.execute('insert ignore into %s(LogId, LogDescription,ErrorFlag) values("%d","%s","%d")'%\
		(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
			
except URLError as log_error:
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.nw_error_status_id,EndTime,current_log_id) )
												
	error_flag = 1
	log_detail = "No Network Connection"
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
				
except Exception, log_error:
	
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.error_status_id,EndTime,current_log_id) )
												
	log_detail = "Error in loading data"
	error_flag = 1
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
	
KomparifyBasePlanSqlscripts.db.commit()
KomparifyBasePlanSqlscripts.db.close()					
					
	
				
					