'''
	Run 4th
	Get the plan url from AllPlanLinks table and extract detailed information about all plans
'''
from bs4 import BeautifulSoup
from mysql.connector import MySQLConnection, Error
import urllib2
import urllib
import re
import json
import KomparifyBaseplanConfig
import KomparifyBasePlanSqlscripts
import KomparifyLogStatusCodes
import time
from urllib2 import HTTPError, URLError

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError, e:
	return False
	
  return True

StartTime = time.strftime("%Y-%m-%d %H:%M:%S")  

try:
	cursor = KomparifyBasePlanSqlscripts.db.cursor()
except Exception, e:
	print e

cursor.execute('insert ignore into %s (StartTime , TaskId, LogStatusId) values ("%s","%d","%s")'%\
												(KomparifyBaseplanConfig.Log, StartTime, KomparifyLogStatusCodes.ko_ldplandet_task_id, KomparifyLogStatusCodes.started_status_id) )

cursor.execute('SELECT LAST_INSERT_ID()')
current_log_ids = cursor.fetchone();
current_log_id = current_log_ids[0]

try:
		cursor.execute('update %s set LogStatusId = "%d" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.running_status_id,current_log_id) )
		error_flag = 0
		log_detail = "Retriving data from AllPlanLinksCrawlerTable"										
		cursor.execute('insert ignore into %s (LogId,LogDescription,ErrorFlag) values("%d","%s","%d")'%\
												(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
		PlanLinkTableCursor = KomparifyBasePlanSqlscripts.db.cursor()
		PlanLinkTableCursor.execute('(select * from %s)' % KomparifyBaseplanConfig.AllPlanLinks)
		PlanLinksTableResults = PlanLinkTableCursor.fetchall()
		
		Id = 0
		for result_rows in PlanLinksTableResults:
			PlanId = result_rows[1]
			PlanName = result_rows[2]
			Operator = result_rows[3]
			Circle = result_rows[4]
			PlanUrl = result_rows[10]
			insertStmt = ""
			PlanUrl = PlanUrl + ".json"
			page=urllib2.urlopen(PlanUrl)
			soup = BeautifulSoup(page.read())
			JsonLink = str(soup)
			colon_count = 0
			comma_count = 0
			JsonLink = JsonLink.replace('\r', ' ')
			JsonLink = JsonLink.replace('\t', ' ')
			JsonLink = JsonLink.replace('\n', ' ')
			JsonLink = JsonLink.replace("\'", '"');
			JsonLink = JsonLink.replace('</space>', ' ')
			comma_count = JsonLink.count(',')
			colon_count = JsonLink.count(':')
			if comma_count >= colon_count:
				JsonLink =JsonLink[::-1].replace(',','',1)[::-1]

			IsJson = is_json(JsonLink)
			
			if IsJson:
				Id = Id + 1
				error_flag = 0
				log_detail = "Inserting detailed info about the plan : " + PlanName + "-" + Operator + "-" + Circle										
				cursor.execute('insert ignore into %s (LogId,LogDescription,ErrorFlag) values("%d","%s","%d")'%\
												(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
				soups = json.loads(JsonLink)
				cursor = KomparifyBasePlanSqlscripts.db.cursor()
				BenefitText = soups["description"]
				if "categories" in soups:
					for MainKey in soups["categories"]:				
							KeyValue = MainKey["key"]
							BenefitType = KeyValue
							for Values in MainKey["values"]:
								BenefitText = Values["key"]
								FreeQuantity = Values["free"]
								BenefitRate = Values["rate"]
								
								print Id
								print PlanId
								updated_on = time.strftime("%Y-%m-%d %H:%M:%S")	
								cursor.execute('insert ignore into %s values("%d","%d","%s","%s"," %s ", " %s ","%s","%s") ON DUPLICATE KEY UPDATE\
								BenefitType = values(BenefitType), BenefitText = values(BenefitText), FreeQuantiy = values(FreeQuantiy),\
								BenefitRate = values(BenefitRate),updated_on = values(updated_on)'% \
								(KomparifyBaseplanConfig.AllPlanDetails, Id,PlanId,BenefitType , BenefitText, FreeQuantity, BenefitRate,\
								KomparifyBasePlanSqlscripts.create_time , updated_on))
								
				else:
					updated_on = time.strftime("%Y-%m-%d %H:%M:%S")	
					cursor.execute('insert ignore into %s values("%d","%d","%s","%s"," %s ", " %s ","%s","%s") ON DUPLICATE KEY UPDATE\
					BenefitType = values(BenefitType), BenefitText = values(BenefitText), FreeQuantiy = values(FreeQuantiy),\
					BenefitRate = values(BenefitRate),updated_on = values(updated_on)'% \
					(KomparifyBaseplanConfig.AllPlanDetails, Id,PlanId,BenefitType , BenefitText, FreeQuantity, BenefitRate,\
					KomparifyBasePlanSqlscripts.create_time , updated_on))
					Id = Id + 1
					
		EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
		cursor.execute('update %s set LogStatusId = "%d" ,EndTime = "%s" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.success_status_id,EndTime,current_log_id) )

		log_detail = str(Id) + " " + "records got refreshed"										
		error_flag = 0
		cursor.execute('insert ignore into %s(LogId, LogDescription,ErrorFlag) values("%d","%s","%d")'%\
		(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )										



except URLError as log_error:
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.nw_error_status_id,EndTime,current_log_id) )
												
	error_flag = 1
	log_detail = "No Network Connection"
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
	
	
												
except Exception as log_error:
	
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.error_status_id,EndTime,current_log_id) )
												
	log_detail = "Error in loading data"
	error_flag = 1
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
	
'''	
except urllib2.URLError as err:
		KomparifyBasePlanSqlscripts.db.rollback()
		print "No net connection"
'''		
KomparifyBasePlanSqlscripts.db.commit()
KomparifyBasePlanSqlscripts.db.close()
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							