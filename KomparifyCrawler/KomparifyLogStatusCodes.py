
from bs4 import BeautifulSoup
from mysql.connector import MySQLConnection, Error
import urllib2
import re
import KomparifyBaseplanConfig
import KomparifyBasePlanSqlscripts
import time 


TaskCode_KO_LDOPCIR = "KO_LDOPCIR"
TaskCode_KO_LDPLANLINK = "KO_LDPLANLINK"
TaskCode_KO_LDPLANDET = "KO_LDPLANDET"


LogStatusStarted = "STARTED"
LogStatusRunning = "RUNNING"
LogStatusSuccess = "SUCCESS"
LogStatusError = "ERROR"
LogStatusNetworkError = "NW_ERROR"



cursor = KomparifyBasePlanSqlscripts.db.cursor()

cursor.execute('select TaskId from %s where TaskCode = "%s"'% (KomparifyBaseplanConfig.Task,TaskCode_KO_LDOPCIR))
task_id = cursor.fetchone();
ko_ldopcir_task_id = task_id[0]

cursor.execute('select TaskId from %s where TaskCode = "%s"'% (KomparifyBaseplanConfig.Task,TaskCode_KO_LDPLANLINK))
task_id = cursor.fetchone();
ko_ldplanlink_task_id = task_id[0]

cursor.execute('select TaskId from %s where TaskCode = "%s"'% (KomparifyBaseplanConfig.Task,TaskCode_KO_LDPLANDET))
task_id = cursor.fetchone();
ko_ldplandet_task_id = task_id[0]

cursor.execute('select TaskId from %s where TaskCode = "%s"'% (KomparifyBaseplanConfig.Task,TaskCode_KO_LDOPCIR))
task_id = cursor.fetchone();
ko_ldopcir_status_id = task_id[0]

cursor.execute('select LogStatusId from %s where LogStatusCode = "%s"'% (KomparifyBaseplanConfig.LogStatus,LogStatusStarted))
status_id = cursor.fetchone();
started_status_id = status_id[0]

cursor.execute('select LogStatusId from %s where LogStatusCode = "%s"'% (KomparifyBaseplanConfig.LogStatus,LogStatusRunning))
status_id = cursor.fetchone();
running_status_id = status_id[0]

cursor.execute('select LogStatusId from %s where LogStatusCode = "%s"'% (KomparifyBaseplanConfig.LogStatus,LogStatusSuccess))
status_id = cursor.fetchone();
success_status_id = status_id[0]

cursor.execute('select LogStatusId from %s where LogStatusCode = "%s"'% (KomparifyBaseplanConfig.LogStatus,LogStatusError))
status_id = cursor.fetchone();
error_status_id = status_id[0]

cursor.execute('select LogStatusId from %s where LogStatusCode = "%s"'% (KomparifyBaseplanConfig.LogStatus,LogStatusNetworkError))
status_id = cursor.fetchone();
nw_error_status_id = status_id[0]
