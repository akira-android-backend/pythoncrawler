'''
	No need to run .Create script for all tables along with table name
'''


#KomparifyBasePlan = "KomparifyBasePlanTable"
KomparifyBasePlan = "KomparifyBasePlan"
RegionOperatorNames = "RegionOperatorNames"
AllPlanLinks = "AllPlanLinks"
AllPlanDetails = "AllPlanDetails"
Task = "Task"
LogStatus = "LogStatus"
Log = "Log"
DetailLog = "DetailLog"




CreateRegionOperatorNames = """CREATE TABLE IF NOT EXISTS %s \
( \
    URLID INT AUTO_INCREMENT PRIMARY KEY, \
	service_id VARCHAR(20),\
    service VARCHAR(32), \
	circle_id VARCHAR(20),\
    circle VARCHAR(50), \
	links VARCHAR(100),\
	created_on DATETIME ,\
	updated_on DATETIME  \
)""" % RegionOperatorNames


CreateAllPlanLinks = """CREATE TABLE IF NOT EXISTS %s \
( \
	UrlId INT,\
	PlanId INT PRIMARY Key,\
	PlanName VARCHAR(200),\
	Operator VARCHAR(100),\
	Circle VARCHAR(100),\
	PlanType VARCHAR(25),\
	TopupType VARCHAR(100),\
	BasePrice Float,\
	Validity VARCHAR(100),\
	Benefits_text VARCHAR(1000) ,\
	PlanUrl VARCHAR(500),\
	created_on DATETIME ,\
	updated_on DATETIME,  \
	FOREIGN KEY (URLID) REFERENCES RegionOperatorNames(URLID) \
)""" % AllPlanLinks


CreateAllPlanDetails = """CREATE TABLE IF NOT EXISTS %s \
( \
	Id INT PRIMARY Key,\
	PlanId INT,\
	BenefitType VARCHAR(100),\
	BenefitText VARCHAR(500),\
	FreeQuantiy VARCHAR (100),\
	BenefitRate VARCHAR (100),\
    created_on DATETIME ,\
	updated_on DATETIME,  \
	FOREIGN KEY (PlanId) REFERENCES AllPlanLinks(PlanId) \
)""" % AllPlanDetails

CreateTask = """CREATE TABLE IF NOT EXISTS %s \
( \
	TaskId INT PRIMARY KEY,\
	TaskCode VARCHAR(100),\
	TaskName VARCHAR(100),\
	TaskDescription VARCHAR (1000)\
)""" % Task


CreateLogStatus = """CREATE TABLE IF NOT EXISTS %s \
( \
	LogStatusId INT PRIMARY KEY,\
	LogStatusCode VARCHAR(100),\
	LogStatusName VARCHAR(500)\

)""" % LogStatus

CreateLog = """CREATE TABLE IF NOT EXISTS %s \
( \
    LogId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
	StartTime  DATETIME,\
	EndTime DATETIME,\
	TaskId INT,\
	LogStatusId INT,\
	FOREIGN KEY (TaskId) REFERENCES Task(TaskId),\
	FOREIGN KEY (LogStatusId) REFERENCES LogStatus(LogStatusId)\
)""" % Log

CreateDetailLog = """CREATE TABLE IF NOT EXISTS %s \
( \
    DetailLogId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
	LogId INT,\
	LogDescription VARCHAR(10000),\
	LogError VARCHAR(1000),\
	ErrorFlag BOOLEAN,\
	FOREIGN KEY (LogId) REFERENCES Log(LogId) \
)""" % DetailLog

InsertTaskTable1 = """INSERT IGNORE INTO Task VALUES (1,"KO_LDOPCIR","Load Operators and Circle","Load operators and circle along and url for each combination of operator and circle from https://www.komparify.com/regions")"""

InsertTaskTable2 = """INSERT IGNORE INTO Task VALUES (2,"KO_LDPLANLINK","Load Links for all plans","Load all the basic details about all plan along with the url for detailed detail for each plan from the site  https://www.komparify.com")"""

InsertTaskTable3 = """INSERT IGNORE INTO Task VALUES (3,"KO_LDPLANDET","Load Detailed info about all plans","Load detailed detail for each plan from the site  https://www.komparify.com")"""

InsertLogStatus1 = """INSERT IGNORE INTO LogStatus VALUES (1,"STARTED","Load Started")"""

InsertLogStatus2 = """INSERT IGNORE INTO LogStatus VALUES (2,"RUNNING","Load RUNNING")"""

InsertLogStatus3 = """INSERT IGNORE INTO LogStatus VALUES (3,"SUCCESS","Load Success")"""

InsertLogStatus4 = """INSERT IGNORE INTO LogStatus VALUES (4,"ERROR","Error while trying to load data")"""

InsertLogStatus5 = """INSERT IGNORE INTO LogStatus VALUES (5,"NW_ERROR","Error in network connection")"""








