'''
	No need to run.Because this script is imported in other script, so when we run that script all imported scripts will run first.
	Sqlscript to create database and all tables
'''
from mysql.connector import (connection)
import mysql.connector
import KomparifyBaseplanConfig
import time
from datetime import date, datetime

log_id = 0

try:
	db = mysql.connector.connect(user='root', password='root',charset = "utf8" )					
	cursor = db.cursor()
	cursor.execute('CREATE DATABASE IF NOT EXISTS KomparifyDb;')
	cursor.execute('USE KomparifyDb;')
	cursor.execute( KomparifyBaseplanConfig.CreateRegionOperatorNames)
	cursor.execute( KomparifyBaseplanConfig.CreateAllPlanLinks)
	cursor.execute( KomparifyBaseplanConfig.CreateAllPlanDetails)
	
	cursor.execute( KomparifyBaseplanConfig.CreateTask)
	cursor.execute( KomparifyBaseplanConfig.CreateLogStatus)
	cursor.execute( KomparifyBaseplanConfig.CreateLog)
	cursor.execute( KomparifyBaseplanConfig.CreateDetailLog)
	
	cursor.execute( KomparifyBaseplanConfig.InsertTaskTable1)
	cursor.execute( KomparifyBaseplanConfig.InsertTaskTable2)
	cursor.execute( KomparifyBaseplanConfig.InsertTaskTable3)
	
	cursor.execute( KomparifyBaseplanConfig.InsertLogStatus1)
	cursor.execute( KomparifyBaseplanConfig.InsertLogStatus2)
	cursor.execute( KomparifyBaseplanConfig.InsertLogStatus3)
	cursor.execute( KomparifyBaseplanConfig.InsertLogStatus4)
	cursor.execute( KomparifyBaseplanConfig.InsertLogStatus5)
	
	
	
	
	
	create_time = time.strftime("%Y-%m-%d %H:%M:%S")
	db.commit()

except mysql.connector.Error as err:
	print err