'''
	Run 2nd
	Crawl the Komparify Website and collect all the network operators, telecom circle and url for each combination of
	operator and circle
'''
from bs4 import BeautifulSoup
from mysql.connector import MySQLConnection, Error
import urllib2
import re
import KomparifyBaseplanConfig
import KomparifyBasePlanSqlscripts
import KomparifyLogStatusCodes
import time 
from urllib2 import HTTPError, URLError

StartTime = time.strftime("%Y-%m-%d %H:%M:%S")

	
try:
	cursor = KomparifyBasePlanSqlscripts.db.cursor()
except Exception, e:
	print e

cursor.execute('insert ignore into %s (StartTime ,TaskId, LogStatusId) values ("%s","%d","%s")'%\
												(KomparifyBaseplanConfig.Log,StartTime,KomparifyLogStatusCodes.ko_ldopcir_task_id,KomparifyLogStatusCodes.started_status_id) )

cursor.execute('SELECT LAST_INSERT_ID()')
current_log_ids = cursor.fetchone();
current_log_id = current_log_ids[0]

try:
		cursor.execute('update %s set LogStatusId = "%d" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.running_status_id,current_log_id) )
		error_flag = 0
		
		url="https://www.komparify.com/regions"
		page=urllib2.urlopen(url)		
		soup = BeautifulSoup(page.read())
	
		log_detail = "parsing the link " + url										
		cursor.execute('insert ignore into %s (LogId,LogDescription,ErrorFlag) values("%d","%s","%d")'%\
												(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
		urlid=1
		link_list=[]
		
		for div1 in soup.findAll("div", {"id": "home-content-container"}):	
			
			for div2 in div1.findAll("div",{"id": "specialcontent"}):
				for div3 in div2.findAll("div",{"class": "centredcontent onemorecontent regionpagecontent new_height"}):
					for div4 in div3.findAll("form",{"id":"new_searchparam"}):
						for selectcircle in div4.findAll('select' , {"class":"region_selector"}):
							for option in selectcircle.find_all('option'):
										circle_value = option['value']
										circle_text = option.text
										if circle_text == "Kolkata / Calcutta":
											circle_text = "kolkata"
										elif circle_text == "Chennai / Madras":
											circle_text = "chennai"
										elif circle_text == "Mumbai / Bombay":
											circle_text = "mumbai"
										elif circle_text == "UP WEST":
											circle_text = "upwest-uttarakhand"
										elif circle_text == "Madhya Pradesh & Chattisgarh":
											circle_text = "Madhya Pradesh & Chattisgarh"
										elif circle_text == "Himachal Pradesh":
											circle_text = "himachal-pradesh"
										elif circle_text == "Andhra Pradesh":
											circle_text = "andhra-pradesh"
										circle_text = circle_text.replace(" ","")
										circle_text = circle_text.replace("&","-")
										for selectservice in div4.findAll('select', {"class":"operator_selector"}):
											for option in selectservice.find_all('option'):
												service_value = option['value']
												service_text = option.text
												
												if service_text == "Tata Docomo CDMA":
													service_text = "tata-photon-plus"
												elif service_text == "Virgin GSM":
													service_text = "virgin"
												service_text = service_text.replace(" ","-");
												if circle_value and service_value:	
													link = "https://www.komparify.com/regions/" + re.sub(r'\W+', '-', circle_text) + "/carriers/" +  service_text+ "?type=prepaid"
													link = link.lower()
													link = link[:-13]
													
													updated_on = time.strftime("%Y-%m-%d %H:%M:%S")
													
													cursor.execute('insert ignore into %s values("%d","%s","%s","%s","%s","%s","%s","%s")on duplicate key update\
													service_id = values(service_id), service = values(service),\
													circle_id = values(circle_id), circle = values(circle), links = values(links),\
													updated_on = values(updated_on)' % \
												(KomparifyBaseplanConfig.RegionOperatorNames , urlid ,service_value, service_text ,circle_value, circle_text , link , KomparifyBasePlanSqlscripts.create_time , updated_on) )
													urlid = urlid + 1
													
													log_detail = "Inserting the url for " + circle_text + "-" + service_text + " plans"
													
													error_flag = 0
													cursor.execute('insert ignore into %s(LogId, LogDescription,ErrorFlag) values("%d","%s","%d")'%\
												(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
													
						link = " "
		
		EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
		cursor.execute('update %s set LogStatusId = "%d" ,EndTime = "%s" where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.success_status_id,EndTime,current_log_id) )
												
		log_detail = str(urlid) + " " + "records got refreshed"										
		error_flag = 0
		cursor.execute('insert ignore into %s(LogId, LogDescription,ErrorFlag) values("%d","%s","%d")'%\
		(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,error_flag) )
		

		
except URLError as log_error:
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.nw_error_status_id,EndTime,current_log_id) )
												
	error_flag = 1
	log_detail = "No Network Connection"
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
	
except Exception, log_error:
	
	EndTime = time.strftime("%Y-%m-%d %H:%M:%S")
	cursor.execute('update %s set LogStatusId = "%d",EndTime = "%s"  where LogId = "%d"'%\
												(KomparifyBaseplanConfig.Log,KomparifyLogStatusCodes.error_status_id,EndTime,current_log_id) )
												
	log_detail = "Error in loading data"
	error_flag = 1
	cursor.execute('insert ignore into %s (LogId, LogDescription,LogError,ErrorFlag)values("%d","%s","%s","%d")'%\
	(KomparifyBaseplanConfig.DetailLog,current_log_id,log_detail,log_error,error_flag) )
	print log_error
	print "eoorr"
'''													
except urllib2.URLError as err:
		KomparifyBasePlanSqlscripts.db.rollback()
		print err
'''		
		
KomparifyBasePlanSqlscripts.db.commit()
KomparifyBasePlanSqlscripts.db.close()
