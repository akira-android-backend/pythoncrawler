
package popupdataextract;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Abi
 */
public class Main {
    
    public static void main(String[] args) {
        try {
            String idd = "4";
            ResultSet rs;
            Connect c = new Connect();
            Connection conn = c.makeConnection();
            Statement st = conn.createStatement();
           // String select_Stmt = "Select id,operator,popupmsg from popupmsgs where id ='"+idd+"';;";
            String select_Stmt = "Select * from popupmsg;";
            rs = st.executeQuery(select_Stmt);
            String msg = null;
            String operator = null;
            String CallCost = null;
            String Duration = null;
            String Balance = null;
            String MobilePlan = null;
            String SmsCost = null;
            String DataCost = null;
            String DataUsage = null;
            String DataBalance = null;
            String id = null;
            Statement insertstmt = conn.createStatement();
            
            while(rs.next())
            {
             msg = rs.getString("msg");
             id = rs.getString("id");
             operator = rs.getString("operator");
            //msg = "[Last call charge for 00:01:16 is from Main Bal:0.800.Available Main Bal:Rs.66.180,21-10-2023.UNLTD LLnR2Rn30 Mins othr RC109, 7 Days, OK]";;
             Popupdataextract object = new Popupdataextract(msg);
            // Popupdataextract object = new Popupdataextract(operator,msg);
            CallCost = object.getcallcost();
            SmsCost = object.getsmscost();
            DataCost = object.getdatacost();
            DataUsage = object.getdatausage();
            DataBalance = object.getdatabalance();
            Balance = object.getbalance();
            Duration = object.getduration();
            MobilePlan = object.getplan();
            //System.out.println(id);
            //System.out.println(Balance);
            //System.out.println(Duration);
            //System.out.println("Success..............................");
            insertstmt.execute("insert ignore into PopupExtractedData values('"+id+"','"+msg+"','"+Duration+"','"+CallCost+"','"+SmsCost+"','"+DataCost+"','"+DataUsage+"','"+DataBalance+"','"+Balance+"','"+MobilePlan+"');");
            
            //System.out.println(id);
            }
        }
        catch(SQLException e){
            System.out.println("Failed");
            e.printStackTrace();
        }
        //PopUpDataExtract1 popupdataextract1 = new PopUpDataExtract1("hi");
        
        
    }
    
    
}
