
package popupdataextract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Abi
 */
public class Popupdataextract{
    
    private String callCost = null;
    private String smsCost = null;
    private String dataCost = null;
    private String dataUsage = null;
    private String dataBalance = null;
    private String callDuration = null;
    private String currentBalance;
    private String mobilePlan = null;
    Popupdataextract(String popupText)        
    {

        popupText = popupText.toLowerCase();
        popupText = popupText.replace("[","").replace("]","").replace(" ","");

        ArrayList<String> call_list = new ArrayList<>(Arrays.asList("callcharge","callcost","chrg","lastcall","duration"));

        ArrayList<String> data_list = new ArrayList<>(Arrays.asList("datacharge","internetcharge","datacost",
                "dataused","datausage","datausage:",
                "2gusage","3gusage","vol:","vol"));

        ArrayList<String> sms_pack_list = new ArrayList<>(Arrays.asList("remainingsms","remsms"));

        ArrayList<String> data_bal_list = new ArrayList<>(Arrays.asList("2gbal","2gbal:","3gbal",
                "3gbal:","dataleft","dataleft:", "bal:","available2gpackbenifit"));

        ArrayList<String> data_used_list =  new ArrayList<>(Arrays.asList("vol","vol:","volused",
                "volused:","2gusage","2gusage:", "3gusage","3gusage:","datausage","datausage:",
                "volumeusedfromsharedaccount:","consumedvolume"));

        ArrayList<String> data_cost_list = new ArrayList<>(Arrays.asList("cost","cost:","chrg",
                "chrg:","charge","charge:"));

        ArrayList<String> balance_list = new ArrayList<>(Arrays.asList("bal","bal:","mainbal",
                "mainbal:","bal-left","bal-left:", "availablebal","accountbalanceis", 
                "remainingbalanceis","maina/cbalance","accountbalanceis","currentbalance"));

        ArrayList<String> call_cost_list = new ArrayList<>(Arrays.asList("callcost","callcost:",
                "callcharge","callcharge:","chrg","chrg:", "frommainbal:","frompromo2","frompromo2:"));

        ArrayList<String> sms_cost_list = new ArrayList<>(Arrays.asList("smswascharged","smscost","smscost:",
                "smscharge","smscharge:","smschargefrompromo2", "smschargefrompromo2:",
                "smscharge from mainbal","smschargefrommainbal:"));

        String cost = "[:]?+[(rs)[0-9.=]+(inr)(min)(sec)?+]+";
        int loop_again = 0;
        for (String calllist : call_list) // Finds call popup msg using calllist keywords
            if((popupText.contains(calllist)))
            {
                Pattern duration_pattern = Pattern.compile("\\d+\\:\\d+\\:\\d+"); //Pattern for duration in this (00:02:03) format
                Pattern duration_pattern1 = Pattern.compile("[duration,duration:,durn,durn:,dur:,dur,lastcallof]\\d+(sec)+"); //Pattern for duration in format line 189sec
                Pattern duration_pattern2 = Pattern.compile("[duration,duration:,durn,durn:,dur:,dur,lastcallof]\\d+(min)+"); //Pattern for duration in format line 189min
                
                Matcher duration_matcher = duration_pattern.matcher(popupText);
                Matcher duration_matcher1 = duration_pattern1.matcher(popupText);
                Matcher duration_matcher2 = duration_pattern2.matcher(popupText);

                if(duration_matcher.find())
                {
                    callDuration = duration_matcher.group(0);
                }
                else if(duration_matcher1.find())
                {
                    callDuration = duration_matcher1.group(0);
                    callDuration = callDuration.replace("duration", "");
                    callDuration = callDuration.replaceAll("[a-z:&&[^sec]]", "");
                }
                else if(duration_matcher2.find())
                {
                    callDuration = duration_matcher2.group(0);
                    callDuration = callDuration.replace("duration", "");
                    callDuration = callDuration.replaceAll("[a-z:&&[^(min)]]", "");
                }

                if(callDuration != null) // Find call cost only if duration is not null
                {
                    for(String callcostlist : call_cost_list)
                    {
                        String callcost = "(" + callcostlist + ")" + cost ; // Forming regex pattern for finding call cost
                                                                            // eg: "(callcost)[:]?+[(rs)[0-9.=]+(inr)(min)(sec)?+]+"
                        Pattern call_cost_pattern = Pattern.compile(callcost);
                        Matcher call_cost_matcher = call_cost_pattern.matcher(popupText);

                        if(call_cost_matcher.find()) //Extract the callcost if the specific pattern is find in popuptext  
                        {                            //eg: catch the text if it contains callcost:rs.67.90
                            callCost = call_cost_matcher.group(0);
                            callCost = callCost.replace(callcostlist, "");    
                            callCost = extractCost(callCost);
                        }
                    }//Callcostlist for ends
                    
                  }//if ends
   
                    
                for (String balancelist : balance_list) //For Loop to find balance using balance_list array
                    {
                        String balance = "(" + balancelist + ")" + cost;//forming regex pattern for balance
                        Pattern balance_pattern = Pattern.compile(balance);
                        Matcher balance_matcher = balance_pattern.matcher(popupText);

                        if (balance_matcher.find()) 
                        {
                            currentBalance = balance_matcher.group(0);
                            currentBalance = currentBalance.replace(balancelist, "");
                            
                            /*
                            If Balance is in minutes or seconds ,Then it says that some plans are activated
                                So Make mobilePlan as plan activated
                            Else 
                                Save the balance as it is.
                            */
                            if(currentBalance.contains("min")||(currentBalance.contains("sec")))
                            {
                                this.mobilePlan = "plan activated : Remaining balance: " + currentBalance ;
                                loop_again = loop_again+1;
                            }
                            else 
                            {
                              currentBalance = extractBalance(currentBalance);  
                            } 
                           
                        }//if ends
                    }//for ends
                
                
                    /*
                    If mobilePlan have some values then try extracting the real balance.
                     */
                    if (mobilePlan != null)
                    {
                        for (String balancelist1 : balance_list) 
                        {
                            String cost1 = "[(rs)[0-9.=]+(inr)?+]+";
                            String balance1 = "(" + balancelist1 + ")" + cost1;//forming regex pattern for balance
                            Pattern balance_pattern1 = Pattern.compile(balance1);
                            Matcher balance_matcher1 = balance_pattern1.matcher(popupText);

                            if (balance_matcher1.find())
                            {
                                currentBalance = balance_matcher1.group(0);
                                currentBalance = currentBalance.replace(balancelist1, ""); 
                                currentBalance = extractBalance(currentBalance);
                            }//if ends
                        }//for ends
                    }                
            }// call ends

        for(String datalist : data_list) // Finding data usage popupmsg using data_list array
        {
            if(popupText.contains(datalist))
            {
                for(String databallist : data_bal_list) //Finds data balance
                {
                    String databal = "[:]?+[0-9.]+[mb|kb| gb]+";
                    databal = "(" + databallist + ")" + databal; //Forms regex pattern for databalance
                    Pattern data_bal_pattern = Pattern.compile(databal);
                    Matcher data_bal_matcher = data_bal_pattern.matcher(popupText);

                    if(data_bal_matcher.find()){
                        dataBalance = data_bal_matcher.group(0);
                        dataBalance = dataBalance.replace(databallist, "");
                    }
                }

                for(String datausedlist : data_used_list) // Finds data usage
                {
                    String datausage = "[:]?+[0-9.]+[mb| kb| gb]+";
                    datausage = "(" + datausedlist + ")" + datausage;//Forms regex pattern for datausage
                    Pattern data_usage_pattern = Pattern.compile(datausage);
                    Matcher data_usage_matcher = data_usage_pattern.matcher(popupText);

                    if(data_usage_matcher.find()){
                        dataUsage = data_usage_matcher.group(0);
                        dataUsage = dataUsage.replace(datausedlist, "");
                        dataUsage = dataUsage.replaceAll("[a-z&&[^mbgk]]", "");
                        dataUsage = dataUsage.replaceAll(":", "").replaceAll(",", "").replaceAll("-","");
                    }
                }

                for(String datacostlist : data_cost_list) // Finds datacost
                {

                    String datacost = "(" + datacostlist + ")" + cost; //Forms regex pattern for datacost
                    Pattern data_cost_pattern = Pattern.compile(datacost);
                    Matcher data_cost_matcher = data_cost_pattern.matcher(popupText);

                    if(data_cost_matcher.find()){
                        dataCost = data_cost_matcher.group(0);
                        dataCost = dataCost.replace(datacostlist, "");
                        dataCost = dataCost.replace("rs.", "").replace("rs","").replace("inr", "");
                        dataCost = extractCost(dataCost);
                    }
                }

                for(String balancelist : balance_list)  // Finds mainbalance
                {

                    String balance = "(" + balancelist + ")" + cost;   //Forms regex pattern for main balance
                    Pattern balance_pattern = Pattern.compile(balance);
                    Matcher balance_matcher = balance_pattern.matcher(popupText);

                    if(balance_matcher.find()){
                        currentBalance = balance_matcher.group(0);
                        currentBalance = currentBalance.replace(balancelist,"");
                        currentBalance = extractBalance(currentBalance);
                    }
                }

            }
        }// data ends

        for (String smslist : sms_cost_list)//Finds sms popupmsg
        {
            if((popupText.contains(smslist)))
            {
                for(String smscostlist : sms_cost_list) // Finds sms cost
                {

                    String smscost = "(" + smscostlist + ")" + cost; //Forms regex pattern for sms cost
                    Pattern sms_cost_pattern = Pattern.compile(smscost);
                    Matcher sms_cost_matcher = sms_cost_pattern.matcher(popupText);

                    if(sms_cost_matcher.find()){
                        smsCost = sms_cost_matcher.group(0);
                        smsCost = smsCost.replace(smscostlist,"");
                        smsCost = smsCost.replace("rs.","").replace("rs","").replace("inr", "");
                        smsCost = extractCost(smsCost);
                    }
                }
                
                /*
                If sms popupmsg does not contain sms cost,then It says that sms pack is activated.
                so enable mobilePlan and save the remaining sms count.
                */
                
                if (smsCost == null)
                {
                  for(String smspacklist : sms_pack_list) 
                {
                    String num = "[:]?+\\d+";
                    String smspack = "(" + smspacklist + ")" + num; //Forms regex pattern for sms remaining count like
                                                                    //eg: Remainingsms:128
                    Pattern sms_pack_pattern = Pattern.compile(smspack);
                    Matcher sms_pack_matcher = sms_pack_pattern.matcher(popupText);

                    if(sms_pack_matcher.find()){
                        mobilePlan = sms_pack_matcher.group(0);
                        mobilePlan = mobilePlan.replaceAll("[a-z:.]","");
                        mobilePlan = "sms pack activated. Remaining sms : " + mobilePlan;
                    }
                }  
                }

                for(String balancelist : balance_list) // Finds main balance
                {
                    String balance = "(" + balancelist + ")" + cost;   //Forms regex pattern for main balance
                    Pattern balance_pattern = Pattern.compile(balance);
                    Matcher balance_matcher = balance_pattern.matcher(popupText);

                    if(balance_matcher.find()){
                        currentBalance = balance_matcher.group(0);
                        currentBalance = currentBalance.replace(balancelist,"");
                        currentBalance = extractBalance(currentBalance);
                    }
                }
            }

        }// sms

//        Log.d("textpop", "call cost: " + this.callCost + " duration: " + this.callDuration +
//                " curBalance: " + this.currentBalance + " mobileplan " + this.mobilePlan +
//                " smsCost: " + this.smsCost + " datacost " + this.dataCost + " dataUsage " +
//                this.dataUsage + " databalance " + this.dataBalance + " promobalance " +
//                this.promoBalance);
        
        
        
        /*
        If the we cannot extract callduration,datausage or smscost from the popup msg,then try extracting the cost spend 
        and save it in smscost.
        
        */
        if ((callDuration == null) && (dataUsage == null) && (smsCost == null))
        {
            for(String callcostlist : call_cost_list)
            {
                String callcost = "(" + callcostlist + ")" + cost ; // Forms regex pattern for callcostfinder
                Pattern call_cost_pattern = Pattern.compile(callcost);
                Matcher call_cost_matcher = call_cost_pattern.matcher(popupText);

                if(call_cost_matcher.find()){
                    smsCost = call_cost_matcher.group(0);
                    smsCost = smsCost.replace(callcostlist, "");
                    smsCost = smsCost.replace("rs.","").replace("inr","").replace("rs", "").replace("=", "");
                    smsCost = extractCost(smsCost);
                }
            }
            for(String balancelist : balance_list) // Finding main balance
            {
                String balance = "(" + balancelist + ")" + cost;   //Forming regex pattern for main balance
                Pattern balance_pattern = Pattern.compile(balance);
                Matcher balance_matcher = balance_pattern.matcher(popupText);

                if(balance_matcher.find()){
                    currentBalance = balance_matcher.group(0);
                    currentBalance = currentBalance.replace(balancelist,"");
                    currentBalance = extractBalance(currentBalance);
                }
            }
        }
    }

    public String extractCost(String balance)
    {
        //remove all unwanted characters from data,sms and call cost
        if(balance!=null) {
            balance = balance.replaceAll("[a-z:-=]","");
            if(balance.startsWith("."))
                balance = balance.replaceFirst(".","");
            if(balance.endsWith(".")){
                int len = balance.length();
                balance = balance.substring(0, len-1);
            }
        }
        return balance;
    }  
    public String extractBalance(String balance)
    {
        /*
                If the balance contains "rs" or "rs." extract the numbers after that
                eg: rs.12.31 
                Then balance = 12.31
        */
        if(balance != null){
            if((balance.contains("rs"))|| (balance.contains("rs."))){
                Pattern cost_pattern = Pattern.compile("[rs,rs.]?+\\d+\\.\\d+");
                Matcher cost_matcher = cost_pattern.matcher(balance);
                if(cost_matcher.find()){
                    balance = cost_matcher.group(0);
                    balance = balance.replaceAll("[a-z:-]", "");

                }
            }//if rs

            else if(balance.contains("inr"))
            {
                /*
                If the balance contains INR then,extract the numbers before that
                eg: 12.31INR
                Balance = 12.31
                */
                String index_string = "inr";
                int index = balance.indexOf(index_string);
                balance = balance.substring(0, index);
                balance = balance.replaceAll("[a-z:-]","");
            }//if inr ends

            if(balance.startsWith("."))
                balance = balance.replaceFirst(".","");
            if(balance.endsWith(".")){
                int len = balance.length();
                balance = balance.substring(0, len-1);
            }
        }
        return balance;
    }
    
       String getduration()
    {
        return callDuration;   
    }
    
    String getcallcost()
    {
        return callCost;
    }
    
    String getsmscost()
    {
        return smsCost;
    }
    
    String getdatacost()
    {
        return dataCost;
    }
    
    String getdatausage()
    {
        return dataUsage;
    }
    
    String getdatabalance()
    {
        return dataBalance;
    }
    
    String getbalance()
    {
        return currentBalance;
    }
    
    String getplan()
    {
        return mobilePlan;
    }

    }
